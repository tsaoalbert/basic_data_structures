#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <iomanip>

using namespace std;

/*
  Implement class Deque_array to simulate the STL container deque using array.

  * Do not modify the *.h file.
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/


class Deque_array {
private:
  int* m_arr = nullptr ; 
  size_t m_head = 0 ; 
  size_t m_size = 0 ;
  size_t m_capacity = 0 ;

private: // helper function
  void copy (  const Deque_array& x ) ;
public:
   // CSTR
   Deque_array (  size_t n ) {
      m_arr = new int [n] ;
      std::fill(m_arr, m_arr+n, 0);
      m_capacity = n;
      m_size = 0;
   }

   // Rules of Big 3: DSTR
   ~Deque_array (  ) {
      delete [] m_arr ;
   }
   // Rules of Big 3: copy cstr
    Deque_array (  const Deque_array& x ) {
      copy (x);
    }
   // Rules of Big 3: assignment operator
   Deque_array& operator= (  const Deque_array& x ) {
      if ( &x!=this ) copy (x);
      return *this;
   }

  inline bool  full() const {
    return (m_size==m_capacity) ;
  }
  inline bool  empty() const {
    return m_size == 0 ;
  }

  // overload the random access operator
  int  operator[](size_t i) const ;
  void push_front(int x ) ;
  void push_back(int x ) ;

  void pop_front() ;
  void pop_back() ;
  
  int front() ;
  int back() ;

  string to_string () const ;
  // friend ostream& operator<< ( ostream& out, Deque_array& d ) { }

};


