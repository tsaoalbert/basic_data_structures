
/*
   
  Implement class Deque_array to simulate the STL container deque.
  
  * Do not modify the *.h file. 
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/


#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <iomanip>
#include "deque.array.h"

using namespace std;

  // random access
  int  Deque_array::operator[](size_t i) const {
    // TODO: fill in the missing codes
    return 0;
  }
  void Deque_array::push_front(int x ) {
    // TODO: fill in the missing codes
  }
  int Deque_array::back() {
    // TODO: fill in the missing codes
    return 0;
  }
  int Deque_array::front() {
    // TODO: fill in the missing codes
    return 0;
  }
  void Deque_array::pop_front() {
    // TODO: fill in the missing codes
  }
  void Deque_array::push_back(int x ) {
    // TODO: fill in the missing codes
  }
  void Deque_array::pop_back() {
    // TODO: fill in the missing codes
  }
  string Deque_array::to_string () const {
    // TODO: fill in the missing codes
    return "";
  }

    void Deque_array::copy (  const Deque_array& x ) {
     if ( m_arr ) { delete [] m_arr; };
      int n = m_capacity = x.m_capacity;
      m_size = x.m_size;
      m_head = x.m_head;
      m_arr = new int [n] ;
      for ( int i = 0; i < m_size; ++i ) {
        m_arr[ i ] = x.m_arr[ (x.m_head+i)%n ] ;
      }
    }

