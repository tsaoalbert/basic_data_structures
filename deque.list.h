#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <list>
#include <iomanip>
#include "deque.vector.h"
#include "deque.array.h"

using namespace std;

/* Re-implement the STL deque container with STL container list */
/*
  Implement class Deque_list to simulate the STL container queue using list.

  * Do not modify the *.h file.
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/

class Deque_list {
private:
  list<int> m_list;

public:
  inline bool  empty() const {
    return m_list.empty()  ;
  }
  inline void push_front(int x ) {
    m_list.push_front (x );
  }
  inline void push_back(int x ) {
    m_list.push_back (x );
  }
  inline int back() {
    return m_list.back() ;
  }
  inline int front() {
    return m_list.front() ;
  }
  inline void pop_front() {
    if ( m_list.empty() ) {
    } else {
      m_list.pop_front ();
    }
  }
  inline void pop_back() {
    if ( m_list.empty() ) {
    } else {
      m_list.pop_back ( );
    }
  }
  // random access
  int  operator[](size_t i) const ;

  string to_string () const {
    ostringstream sout;
    for ( auto& x: m_list ) {
      sout << setw(3) << std::to_string (x) << " " ; 
    } ;
    return sout.str();
  }

};

