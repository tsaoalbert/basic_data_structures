
/*
   
  Implement class Queue_deque to simulate the STL container queue using deque.
  
  * Do not modify the *.h file. 
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/

#pragma once 

#include <deque>
#include <iomanip>

using namespace std;

class Stack_deque {
private:
  deque<int> m_deque;
public:
  void pop () ;
  void push ( int ) ;
  int top () const ;
  string to_string () const ;
};

