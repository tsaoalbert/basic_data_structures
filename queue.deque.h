

/*
   
  Implement class Stack_deque to simulate the STL container stack using deque.
  
  * Do not modify the *.h file. 
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/

#pragma once 
#include <deque>
#include <iomanip>

using namespace std;

class Queue_deque {
private:
  deque<int> m_deque;
public:
  void pop () ;
  void push ( int ) ;
  int front () const ;
  int back () const ;
  int size () const ;
  int empty () const ;
  string to_string () const ;
};

