
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <list>
#include <iomanip>
#include "deque.vector.h"
#include "deque.array.h"
#include "deque.list.h"
#include "stack.deque.h"
#include "queue.deque.h"

using namespace std;

void test_Queue_deque() {
  cout << endl << __func__ << ":" << endl;
  Queue_deque q;
  size_t n = 10;
  for (size_t i = 0 ; i < n; ++i ) {
    q.push(i*i);
  }
  for (size_t i = 0 ; i < n/2; ++i ) {
    q.pop();
  }
  for (size_t i = 0 ; i < n/2; ++i ) {
    q.push(-i*i);
  }
  cout << q.to_string () << endl;
  
}
void test_Stack_deque() {
  cout << endl << __func__ << ":" << endl;
  Stack_deque stk;
  size_t n = 10;
  for (size_t i = 0 ; i < n; ++i ) {
    stk.push(i*i);
  }
  for (size_t i = 0 ; i < n/2; ++i ) {
    stk.pop();
  }
  for (size_t i = 0 ; i < n/2; ++i ) {
    stk.push(-i*i);
  }
  cout << stk.to_string () << endl;
  
}

void test_Deque_array()
{
  cout << endl << __func__ << ":" << endl;
  size_t n = 10;
  Deque_array q ( n );
  for (size_t i = 0 ; i < n/2; ++i ) {
    q.push_back ( i );
    q.push_front ( -i );
    cout << q.to_string () << endl;
  }
}

void test_Deque_list ()
{
  cout << endl << __func__ << ":" << endl;

  size_t n = 10;
  Deque_list q ;
  for (size_t i = 0 ; i < n/2; ++i ) {
    q.push_back ( i );
    q.push_front ( -i );
    cout << q.to_string () << endl;
  }
}


void test_Deque_vector()
{
  cout << endl << __func__ << ":" << endl;
  size_t n = 10;
  Deque_vector q ( n );
  for (size_t i = 0 ; i < n; ++i ) {
    q.push_back ( i );
    q.push_front ( -i );
    cout << q.to_string () << endl;
  }
}

int main(int argc, char* argv[])
{
    test_Deque_array ();
    test_Deque_list ();
    test_Deque_vector ();
    test_Stack_deque ();
    test_Queue_deque ();
}
