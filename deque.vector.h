#pragma once 

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <iomanip>

using namespace std;

/* Re-implement the STL deque container with STL container vector  */
/*
  Implement class Deque_vector to simulate the STL container queue using vecgtor.

  * Do not modify the *.h file.
  * Implement the fuctions in the *.cpp files.
  * Look for the key words TODO to fill in the missing codes.

*/

class Deque_vector {
private:
  vector<int> m_vec;
  size_t m_head = 0 ; 
  size_t m_size = 0 ;

public:
  inline Deque_vector (  size_t n=0 ) {
      m_vec.reserve (n); 
  }
  inline bool  full() const {
    return m_size==m_vec.capacity();
  }

  inline bool  empty() const {
    return m_size == 0 ;
  }

  // random access
  int  operator[](size_t i) const ;
  void doubleCapacity () ;
  void push_front(int x ) ;
  void push_back(int x ) ;
  void pop_front() ;
  void pop_back() ;
  int  back() ;
  int  front() ;

  string to_string () const ;

};

